/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 *
 * Übungsrobot
 * Tobias Hirn
 * ET12B
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include <math.h>
#include <stdlib.h>

//--------------------------------------------------------------------------
//                           Class Robot25
//--------------------------------------------------------------------------

class Robot25 : public Driver
{
public:
    // Konstruktor
    Robot25()
    {
        // Der Name des Robots
        m_sName = "Robot25";
        // Namen der Autoren
        m_sAuthor = "Tobias";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLUE;
        m_iTailColor = oBROWN;
        m_sBitmapName2D = "car_lblue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

    // Winkelberechnung
    // in: s.vn, s.v, PI
    // Berechnung des alpha Wertes für die Lenkung (Gradmaß)
    // out: alpha

    double Winkelberechnung (double vn, double v, double PI)
    {
        double alpha = 0.0;
        alpha =(-asin(vn/v))*2*PI;
        return alpha;
    }

    // Korrektur
    // in: s.to_lft, s.to_rgt, Spur, Streckenbreite, s.cur_rad
    // Korrektur des alpha Wertes um gewünschte Spur zu fahren
    // Korrekturwert

    double Korrektur (double a_links, double a_rechts, double Streckenbreite, double Radius)
    {
        double offset_l = 0.0;
        double offset_r = 0.0;
        double Spur = 0.0;
        double Korrekturwert = 0.0;

        if (Radius == 0)
        {
            Spur = Streckenbreite/2;
            offset_l = 0.5;
            offset_r = 0.5;
        }
        else if (Radius < 0)
        {
            Spur = 0.001*Streckenbreite;
            offset_l = 0.0;
            offset_r = 1.0;
        }
        else if (Radius > 0)
        {
            Spur = 0.001*Streckenbreite;
            offset_l = 1.0;
            offset_r = 0.0;
        }

        Korrekturwert = offset_l*((a_links-Spur)/Streckenbreite) - offset_r*((a_rechts-Spur)/Streckenbreite);
        return Korrekturwert;
    }

    // max_Kurvengeschwindigkeit
    // in: Radius zum Streckenrand, Konstante MYU, Konstante g, Radius der folgenden Kurve, Hilfsvariable i für Bestimmung Radius, Streckenbreite,
    //     Kurve_l_r: Variable zur Bestimmung einer Kombination aus Links- und Rechtskurve
    // Berechnung der maximmalen Kurvengeschwindigkeit
    // out: Geschwindigkeit

    double max_Kurvengeschwindigkeit (double RAD1, double MYU, double g, double RAD2, int i, double Streckenbreite, int Kurve_l_r)
    {
        double Geschwindigkeit;
        double G = g*3.2808;

        if (i=0)                                  // Auto befindet sich auf einer Gerade
        Geschwindigkeit = sqrt(G*MYU*RAD2);

        else                                      // Auto befindet sich in einer Kurve
        Geschwindigkeit = sqrt(G*MYU*RAD1);
        {
            if (Streckenbreite <= 80)
            {
                if (Streckenbreite ==75 && Kurve_l_r ==1)
                return 0.3*Geschwindigkeit;

                else if (Streckenbreite ==75 && Kurve_l_r ==0)
                return Geschwindigkeit;

                else
                return 1.8*Geschwindigkeit;
            }
            else
            {
                return 20*Geschwindigkeit;
            }
        }

    }

    // Bremsweg
    // in: aktuelle Geschwindigkeit, maximale Kurvengeschwindigkeit, Verzögerung, Streckenbreite,
    //     Kurve_l_r: Variable zur Bestimmung einer Kombination aus Links- und Rechtskurve
    // Berechnung des benötigten Bremsweges
    // out: Bremsweg

    double Bremsweg(double aktl_Geschwindigkeit, double Kurvengeschwindigkeit, double Verzoegerung,double Streckenbreite,int Kurve_l_r)
    {
        double Bremsweg;
        double diff;

        diff = Kurvengeschwindigkeit - aktl_Geschwindigkeit;
        if (diff > 0)
        return 0.0;

       if (diff < 0)
        {
         if (Streckenbreite <= 80)
         {
             if (Kurve_l_r = 1)
             {
                 Bremsweg = (aktl_Geschwindigkeit + 0.5*diff)*diff/Verzoegerung;
                 return 1.1*Bremsweg;
             }
             else
                 return 1.0*Bremsweg;
         }
         else
            {
                Bremsweg = (aktl_Geschwindigkeit + 0.5*diff)*diff/Verzoegerung;
                return 0.9*Bremsweg;
            }
        }

    }

    // Bremsen
    // in: aktuelle Geschwindigkeit, maximale Kurvengeschwindigkeit, Streckenbreite,
    //     Kurve_l_r: Variable zur Bestimmung einer Kombination aus Links- und Rechtskurve
    // Bremsvorgang vor einer Kurve
    // out: neue_Geschwindigkeit

    double Bremsen (double aktl_Geschwindigkeit, double Kurvengeschwindigkeit, double Streckenbreite, int Kurve_l_r)
    {
        double neue_Geschwindigkeit = 0.0;

        if (aktl_Geschwindigkeit > 1.05*Kurvengeschwindigkeit)

            if (Streckenbreite <= 70)
            neue_Geschwindigkeit = aktl_Geschwindigkeit*0,8;

            else if (Streckenbreite ==75 && Kurve_l_r == 1)
            return 0.4*aktl_Geschwindigkeit;

            else
            neue_Geschwindigkeit = aktl_Geschwindigkeit*0.9;

        if (aktl_Geschwindigkeit < Kurvengeschwindigkeit)
            neue_Geschwindigkeit = aktl_Geschwindigkeit*1,5;

        return neue_Geschwindigkeit;
    }

    // Abfrage_r_l_Kurve
    // in: s.cur_rad && s.nex_rad
    // Funktion, die abfragt, ob auf eine rechte Kurve eine Links Kurve folgt
    // out: 1 || 0

    int Abfrage_r_l_Kurve (double RAD1, double RAD2)
    {
        int i = 0.0;
        if (RAD1 < 0 && RAD2 > 0)
        i = 1;
        else i = 0;
        return i;
    }

    con_vec drive(situation& s)
    {
        // hier wird die Logik des Robots implementiert
        con_vec result = CON_VEC_EMPTY;

    // Konstanten
    const static double MYU = 0.65;
    const static double PI  = 3.14159;
    const static double g   = 9.81;
    const static double max_Geschwindigkeit = 250;
    const static double negative_Beschleunigung = -40;

    //Variablen
    int Kurve = 0;
    double alpha = 0.0;
    double Streckenbreite = 0.0;
    double vc = 0.0;

    if (s.starting)
    {
        result.fuel_amount = MAX_FUEL;
    }

    Streckenbreite = s.to_lft + s.to_rgt;

    // Lenkung
    alpha = Winkelberechnung(s.vn,s.v,PI) + Korrektur(s.to_lft,s.to_rgt,Streckenbreite,s.cur_rad);

    double v = 0.0;

    // Funktion speziell für Strecke Silverstone und Brazil, um scharfe Kurve ohne Probleme zu durchfahren
    Abfrage_r_l_Kurve(s.cur_rad,s.nex_rad);

    //Geschwindigkeitberechnung
    if (s.cur_rad == 0)
   {
        Kurve = 0;
        if (s.nex_rad =! 0)
        {
            v = max_Kurvengeschwindigkeit(s.cur_rad,MYU,g,s.nex_rad,Kurve,Streckenbreite,Abfrage_r_l_Kurve(s.cur_rad,s.nex_rad));

            if(s.to_end < Bremsweg(s.v,v,negative_Beschleunigung,Streckenbreite,Abfrage_r_l_Kurve(s.cur_rad,s.nex_rad)))
            vc = Bremsen(s.v,v,Streckenbreite,Abfrage_r_l_Kurve(s.cur_rad,s.nex_rad));

            else
            vc = max_Geschwindigkeit;
        }

        else
        vc = max_Geschwindigkeit;

    }

    else if (s.cur_rad > 0)
    {
        Kurve = 1;
        vc = max_Kurvengeschwindigkeit(s.cur_rad,MYU,g,s.nex_rad,Kurve,Streckenbreite,Abfrage_r_l_Kurve(s.cur_rad,s.nex_rad));
    }

    else if  (s.cur_rad < 0)
    {
        Kurve = 1;
        vc = max_Kurvengeschwindigkeit((-s.cur_rad),MYU,g,s.nex_rad,Kurve,Streckenbreite,Abfrage_r_l_Kurve(s.cur_rad,s.nex_rad));
    }

    //Boxenstop => Reparatur + Tanken
    if (s.fuel < 5.0)
    {
        result.request_pit   = 1;
        result.repair_amount = s.damage;
        result.fuel_amount = MAX_FUEL;
    }

    result.vc = vc;
    result.alpha = alpha;
    return result;

    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot25Instance()
{
    return new Robot25();
}
